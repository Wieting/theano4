python gouda/word-rep.py rep-300-word-model "../../data/sl999_XXL_data.txt" "simlex" "word-model-jhu9.pickle2" > word_cmds_rep.txt
python gouda/big-rep.py rep-1200-word-model "../../data/sl999_XXL_data.txt" "simlex" "big-model-12002.pickle2" 1200 >> word_cmds_rep.txt
python gouda/big-rep.py rep-2400-word-model "../../data/sl999_XXL_data.txt" "simlex" "big-model-24002.pickle2" 2400 >> word_cmds_rep.txt
python gouda/big-rep.py rep-4800-word-model "../../data/sl999_XXL_data.txt" "simlex" "big-model-48003.pickle2" 4800 >> word_cmds_rep.txt