import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
repfile = sys.argv[4]
big = sys.argv[5]
dim = 300

LC = [1E-3,1E-4,1E-5,1E-6]
batchsize = [25,50,100]
memsize = [50,150,300]

train = "../../datasets_tokenized/sicktrain"
dev = "../../datasets_tokenized/sickdev"
test = "../../datasets_tokenized/sicktest"
minval = 1
maxval = 5

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-updatewords False -hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype big -evaluate True \
-epochs 20 -minval {7} -maxval {8} -task sim -traintype rep -regfile ../../data/{9} -big {10}".format(wordstem,wordfile,
                                                                                       fname,dim,train,dev,test,minval,maxval,repfile,big)

for i in batchsize:
    for k in LC:
        for l in memsize:
            next_cmd = cmd + " -batchsize {0} -LC {1} -memsize {2}".format(i,k,l)
            print next_cmd

train = "../../datasets_tokenized/sicktrain-ent"
dev = "../../datasets_tokenized/sickdev-ent"
test = "../../datasets_tokenized/sicktest-ent"

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-updatewords False -hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype big -evaluate True \
-epochs 20 -minval {7} -maxval {8} -task ent -traintype rep -regfile ../../data/{9} -big {10}".format(wordstem,wordfile,
                                                                                                              fname,dim,train,dev,test,minval,maxval,repfile,big)
for i in batchsize:
    for k in LC:
        for l in memsize:
            next_cmd = cmd + " -batchsize {0} -LC {1} -memsize {2}".format(i,k,l)
            print next_cmd

train = "../../datasets_tokenized/sentiment-train"
dev = "../../datasets_tokenized/sentiment-dev"
test = "../../datasets_tokenized/sentiment-test"

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-updatewords True -hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype big_sentiment -evaluate True \
-epochs 20 -minval {7} -maxval {8} -traintype rep -task sentiment -regfile ../../data/{9} -big {10}".format(wordstem,wordfile,
                                                                             fname,dim,train,dev,test,minval,maxval,repfile,big)

for i in batchsize:
    for k in LC:
        for l in memsize:
            next_cmd = cmd + " -batchsize {0} -LC {1} -memsize {2}".format(i,k,l)
            print next_cmd