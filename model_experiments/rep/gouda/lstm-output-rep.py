import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300

LC = [1E-3,1E-4,1E-5,1E-6]
batchsize = [25,50,100]
memsize = [50,150,300]

train = "../../datasets_tokenized/sicktrain"
dev = "../../datasets_tokenized/sickdev"
test = "../../datasets_tokenized/sicktest"
minval = 1
maxval = 5

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype lstm -evaluate True \
-epochs 20 -peephole True \
-minval {7} -maxval {8} -task sim -traintype rep -regfile ../../data/lstm-outgate-jhu-pickle.pickle".format(wordstem,wordfile,
                                fname,dim,train,dev,test,minval,maxval)

for i in batchsize:
    for m in LC:
        for l in memsize:
            next_cmd = cmd + " -updatewords False -outgate True -batchsize {0} -LC {1} -memsize {2}".format(i,m,l)
            print next_cmd

train = "../../datasets_tokenized/sicktrain-ent"
dev = "../../datasets_tokenized/sickdev-ent"
test = "../../datasets_tokenized/sicktest-ent"
minval = 1
maxval = 5

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype lstm -evaluate True \
-epochs 20 -peephole True \
-minval {7} -maxval {8} -task ent -traintype rep -regfile ../../data/lstm-outgate-jhu-pickle.pickle".format(wordstem,wordfile,
                                                                                                          fname,dim,train,dev,test,minval,maxval)

for i in batchsize:
    for m in LC:
        for l in memsize:
            next_cmd = cmd + " -updatewords False -outgate True -batchsize {0} -LC {1} -memsize {2}".format(i,m,l)
            print next_cmd