import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300

LC = [1E-3,1E-4,1E-5,1E-6]
LRW = [1E-1,1E-2,1E-3,1E-4,1E-5,1E-6,1E-7,1E-8]
LRC = [1E-1,1E-2,1E-3,1E-4,1E-5,1E-6]
batchsize = [25,50,100]
memsize = [50,150,300]

train = "../../datasets_tokenized/sicktrain"
dev = "../../datasets_tokenized/sickdev"
test = "../../datasets_tokenized/sicktest"
minval = 1
maxval = 5

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype proj -evaluate True \
-epochs 20 -nonlinearity 1 -minval {7} -task sim -maxval {8} -traintype reg -regfile ../../data/proj-jhu-pickle.pickle".format(wordstem,wordfile,
                                                           fname,dim,train,dev,test,minval,maxval)
for i in batchsize:
    for m in LC:
        for l in memsize:
            for k in LRC:
                for j in LRW:
                    next_cmd = cmd + " -updatewords True -batchsize {0} -LRW {1} -LC {2} -LRC {3} -memsize {4}".format(i,j,m,k,l)
                    print next_cmd
                #next_cmd = cmd + " -updatewords False -batchsize {0}  -LC {1} -LRC {2} -memsize {3}".format(i,m,k,l)

train = "../../datasets_tokenized/sicktrain-ent"
dev = "../../datasets_tokenized/sickdev-ent"
test = "../../datasets_tokenized/sicktest-ent"
minval = 1
maxval = 5

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype proj -evaluate True \
-epochs 20 -nonlinearity 1 -minval {7} -task ent -maxval {8} -traintype reg -regfile ../../data/proj-jhu-pickle.pickle".format(wordstem,wordfile,
                                                                                                                            fname,dim,train,dev,test,minval,maxval)
for i in batchsize:
    for m in LC:
        for l in memsize:
            for k in LRC:
                for j in LRW:
                    next_cmd = cmd + " -updatewords True -batchsize {0} -LRW {1} -LC {2} -LRC {3} -memsize {4}".format(i,j,m,k,l)
                    print next_cmd
#next_cmd = cmd + " -updatewords False -batchsize {0}  -LC {1} -LRC {2} -memsize {3}".format(i,m,k,l)