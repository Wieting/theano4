import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300

LC = [1E-3,1E-4,1E-5,1E-6]
LRW = [1E-1,1E-2,1E-3,1E-4,1E-5,1E-6,1E-7,1E-8]
LRC = [1E-1,1E-2,1E-3,1E-4,1E-5,1E-6]
batchsize = [25,50,100]
memsize = [50,150,300]

train = "../../datasets_tokenized/sicktrain"
dev = "../../datasets_tokenized/sickdev"
test = "../../datasets_tokenized/sicktest"
minval = 1
maxval = 5

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype lstm -evaluate True \
-epochs 20 -peephole True \
-minval {7} -maxval {8} -task sim -traintype reg -regfile ../../data/lstm-nooutgate-jhu-pickle.pickle".format(wordstem,wordfile,
                                fname,dim,train,dev,test,minval,maxval)

for i in batchsize:
    for m in LC:
        for l in memsize:
            for k in LRC:
                for j in LRW:
                    next_cmd = cmd + " -updatewords True -outgate False -batchsize {0} -LRW {1} -LC {2} -memsize {3} -LRC {4}".format(i,j,m,l,k)
                    print next_cmd
                    #next_cmd = cmd + " -updatewords True -outgate False -batchsize {0} -LW {1} -LC {2} -memsize {3}".format(i,j,k,l)
                    #print next_cmd

train = "../../datasets_tokenized/sicktrain-ent"
dev = "../../datasets_tokenized/sickdev-ent"
test = "../../datasets_tokenized/sicktest-ent"
minval = 1
maxval = 5

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype lstm -evaluate True \
-epochs 20 -peephole True \
-minval {7} -maxval {8} -task ent -traintype reg -regfile ../../data/lstm-nooutgate-jhu-pickle.pickle".format(wordstem,wordfile,
                                                                                                           fname,dim,train,dev,test,minval,maxval)

for i in batchsize:
    for m in LC:
        for l in memsize:
            for k in LRC:
                for j in LRW:
                    next_cmd = cmd + " -updatewords True -outgate False -batchsize {0} -LRW {1} -LC {2} -memsize {3} -LRC {4}".format(i,j,m,l,k)
                    print next_cmd
#next_cmd = cmd + " -updatewords True -outgate False -batchsize {0} -LW {1} -LC {2} -memsize {3}".format(i,j,k,l)
#print next_cmd