f = open('all_cmds.txt','r')
lines = f.readlines()

ct = 0
cmds = []
while ct < len(lines):
    if ct % 5000 == 0 and ct > 0:
        idx = ct / 5000
        fout = open('batch-cmds-'+str(idx)+'.txt','w')
        for i in cmds:
            fout.write(i.strip()+'\n')
        fout.close()
        fname = 'slurm-script-'+str(idx)+'.txt'
        fout = open(fname,'w')
        fout.write('#!/bin/bash\n')
        fout.write('#SBATCH --partition=contrib-cpu --cpus-per-task=1 --array=1-'+str(len(cmds))+'\n')
        fout.write('bash -c "`sed "${SLURM_ARRAY_TASK_ID}q;d" '+'batch-cmds-'+str(idx)+'.txt'+'`"')
        fout.close()
        cmds = []
    cmds.append(lines[ct])
    ct += 1

fout = open('batch-cmds-last.txt','w')
for i in cmds:
    fout.write(i.strip()+'\n')
fout.close()
fname = 'slurm-script-last.txt'
fout = open(fname,'w')
fout.write('#!/bin/bash\n')
fout.write('#SBATCH --partition=contrib-cpu --cpus-per-task=1 --array=1-'+str(len(cmds))+'\n')
fout.write('bash -c "`sed "${SLURM_ARRAY_TASK_ID}q;d" '+'batch-cmds-last.txt'+'`"')
fout.close()