from glob import glob

def getScore(flist):
    dd_ent = {}
    dd_sentiment = {}
    dd_sim = {}
    for j in flist:
        f=open(j,'r')
        lines = f.readlines()
        best_sentiment = -1
        best_ent = -1
        best_sim = -1
        task = None
        for i in lines:
            l = i
            if '_train.py' in i:
                name = i
                if 'sentiment' in i:
                    task = "sentiment"
                if "task\', \'sim\'" in i:
                    task = "sim"
                if "task\', \'ent\'" in i:
                    task = "ent"
            if 'evaluation' in i:
                if task == "ent":
                    ii=i.split()
                    s = float(ii[1])
                    t = float(ii[2])
                    if s > best_ent:
                        dd_ent[name] = (s,l)
                        best_ent = s
                if task == "sentiment":
                    ii=i.split()
                    s = float(ii[1])
                    t = float(ii[2])
                    if s > best_sentiment:
                        dd_sentiment[name] = (s,l)
                        best_sentiment = s
                if task == "sim":
                    ii=i.split()
                    s = float(ii[1])
                    t = float(ii[3])
                    if s > best_sim:
                        dd_sim[name] = (s,l)
                        best_sim = s

    print len(flist)
    dd_sorted_ent=sorted(dd_ent.items(), key=lambda x: x[1][0], reverse=True)
    fout = open("ent_results.txt","w")
    for i in dd_sorted_ent:
        fout.write(i[0]+str(i[1][0])+i[1][1]+"\n")
    fout.close()
    
    dd_sorted_sim=sorted(dd_sim.items(), key=lambda x: x[1][0], reverse=True)
    fout = open("sim_results.txt","w")
    for i in dd_sorted_sim:
        fout.write(i[0]+str(i[1][0])+i[1][1]+"\n")
    fout.close()

    dd_sorted_sentiment=sorted(dd_sentiment.items(), key=lambda x: x[1][0], reverse=True)
    fout = open("sentiment_results.txt","w")
    for i in dd_sorted_sentiment:
        fout.write(i[0]+str(i[1][0])+i[1][1]+"\n")
    fout.close()

flist = glob("slurm-*.out")
getScore(flist)