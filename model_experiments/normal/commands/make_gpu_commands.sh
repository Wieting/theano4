echo "" > gpu_cmds.txt
python gouda/lstm-gpu.py gpu-lstm-model "../../data/sl999_XXL_data.txt" "simlex" >> gpu_cmds.txt
python gouda/dan-gpu.py gpu-dan-model "../../data/sl999_XXL_data.txt" "simlex" >> gpu_cmds.txt
python gouda/proj-gpu.py gpu-proj-model "../../data/sl999_XXL_data.txt" "simlex" >> gpu_cmds.txt
python gouda/rnn-gpu.py gpu-rnn-model "../../data/sl999_XXL_data.txt" "simlex" >> gpu_cmds.txt
python gouda/word-gpu.py gpu-word-model "../../data/sl999_XXL_data.txt" "simlex" >> gpu_cmds.txt