import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300

LC = [1E-3,1E-4,1E-5,1E-6]
LW = [1E-3,1E-4,1E-5,1E-6,1E-7,1E-8]
batchsize = [25,50,100]
memsize = [50,150,300]
nl = [3]
numlayers = [1,2]

train = "../../datasets_tokenized/sicktrain"
dev = "../../datasets_tokenized/sickdev"
test = "../../datasets_tokenized/sicktest"
minval = 1
maxval = 5

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype dan -evaluate True \
-epochs 10 -minval {7} -maxval {8} -traintype normal -task sim".format(wordstem,wordfile,
                                                           fname,dim,train,dev,test,minval,maxval)
for i in batchsize:
    for m in LC:
        for l in memsize:
            for nonl in nl:
                for num in numlayers:
                    for j in LW:
                        next_cmd = cmd + " -updatewords True -batchsize {0} -LW {1} -LC {2} -memsize {3} -nonlinearity {4} -numlayers {5}".format(i,j,m,l,nonl,num)
                        print next_cmd
                    next_cmd = cmd + " -updatewords False -batchsize {0} -LC {1} -memsize {2} -nonlinearity {3} -numlayers {4}".format(i,j,l,nonl,num)
                    print next_cmd

train = "../../datasets_tokenized/sicktrain-ent"
dev = "../../datasets_tokenized/sickdev-ent"
test = "../../datasets_tokenized/sicktest-ent"
minval = 1
maxval = 5

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype dan -evaluate True \
-epochs 10 -minval {7} -maxval {8} -traintype normal -task ent".format(wordstem,wordfile,
                                                                                       fname,dim,train,dev,test,minval,maxval)
for i in batchsize:
    for m in LC:
        for l in memsize:
            for nonl in nl:
                for num in numlayers:
                    for j in LW:
                        next_cmd = cmd + " -updatewords True -batchsize {0} -LW {1} -LC {2} -memsize {3} -nonlinearity {4} -numlayers {5}".format(i,j,m,l,nonl,num)
                        print next_cmd
                    next_cmd = cmd + " -updatewords False -batchsize {0} -LC {1} -memsize {2} -nonlinearity {3} -numlayers {4}".format(i,j,l,nonl,num)
                    print next_cmd

train = "../../datasets_tokenized/sentiment-train"
dev = "../../datasets_tokenized/sentiment-dev"
test = "../../datasets_tokenized/sentiment-test"
minval = 1
maxval = 5

cmd = "sh train_gouda.sh -wordstem {0} -wordfile {1} -outfile {2} \
-hiddensize 300 -traindata {4} -devdata {5} \
-testdata {6} -layersize {3} -save False -nntype dan_sentiment -evaluate True \
-epochs 10 -minval {7} -maxval {8} -traintype normal -task sentiment".format(wordstem,wordfile,
                                                                                       fname,dim,train,dev,test,minval,maxval)
for i in batchsize:
    for m in LC:
        for l in memsize:
            for nonl in nl:
                for num in numlayers:
                    for j in LW:
                        next_cmd = cmd + " -updatewords True -batchsize {0} -LW {1} -LC {2} -memsize {3} -nonlinearity {4} -numlayers {5}".format(i,j,m,l,nonl,num)
                        print next_cmd
                    next_cmd = cmd + " -updatewords False -batchsize {0} -LC {1} -memsize {2} -nonlinearity {3} -numlayers {4}".format(i,j,l,nonl,num)
                    print next_cmd