f = open('gpu_cmds2_rand.txt','r')
lines = f.readlines()

ct = 0
cmds = []
while ct < len(lines):
    if ct % 4000 == 0 and ct > 0:
        idx = ct / 4000
        fout = open('batch-cmds-gpu-'+str(idx)+'.txt','w')
        for i in cmds:
            fout.write(i.strip()+'\n')
        fout.close()
        fname = 'slurm-script-gpu-'+str(idx)+'.txt'
        fout = open(fname,'w')
        fout.write('#!/bin/bash\n')
        fout.write('#SBATCH --partition=contrib-gpu --cpus-per-task=1 --array=1-'+str(len(cmds))+'\n')
        fout.write('bash -c "`sed "${SLURM_ARRAY_TASK_ID}q;d" '+'batch-cmds-gpu-'+str(idx)+'.txt'+'`"')
        fout.close()
        cmds = []
    cmds.append(lines[ct])
    ct += 1

fout = open('batch-cmds-gpu-last.txt','w')
for i in cmds:
    fout.write(i.strip()+'\n')
fout.close()
fname = 'slurm-script-gpu-last.txt'
fout = open(fname,'w')
fout.write('#!/bin/bash\n')
fout.write('#SBATCH --partition=contrib-gpu --cpus-per-task=1 --array=1-'+str(len(cmds))+'\n')
fout.write('bash -c "`sed "${SLURM_ARRAY_TASK_ID}q;d" '+'batch-cmds-gpu-last.txt'+'`"')
fout.close()