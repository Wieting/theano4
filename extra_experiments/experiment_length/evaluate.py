from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
from scipy.stats import pearsonr
from utils import lookupIDX
import numpy as np

def getSeqs(p1,p2,model,words):
    p1 = p1.split()
    p2 = p2.split()
    X1 = []
    X2 = []
    for i in p1:
        X1.append(lookupIDX(words,i))
    for i in p2:
        X2.append(lookupIDX(words,i))
    return X1, X2

def getScore(p1,p2,model,words):
    p1 = p1.split()
    p2 = p2.split()
    X1 = []
    X2 = []
    for i in p1:
        X1.append(lookupIDX(words,i))
    for i in p2:
        X2.append(lookupIDX(words,i))
    seqs = [X1]
    x1,m1 = model.prepare_data(seqs)
    seqs = [X2]
    x2,m2 = model.prepare_data(seqs)
    scores = model.scoring_function(x1,x2,m1,m2)
    scores = np.squeeze(scores)
    return float(scores)

def getCorrelation(model,words,examples):
    preds = []
    golds = []
    seq1 = []
    seq2 = []
    for i in examples:
        X1, X2 = getSeqs(i[0],i[1],model,words)
        golds.append(i[2])
        x1,m1 = model.prepare_data([X1])
        x2,m2 = model.prepare_data([X2])
        scores = model.scoring_function(x1,x2,m1,m2)
        p = np.squeeze(scores)
        preds.append(p)
    return pearsonr(preds,golds)[0]

def getExamples(f):
    f = open(f,'r')
    lines = f.readlines()
    lis = []
    for i in lines:
        i = i.split("\t")
        p1 = i[0]; p2 = i[1]; score = float(i[2])
        n1 = len(p1.split()); n2 = len(p2.split())
        n = max(n1,n2)
        lis.append((p1,p2,score,n))
    return lis


def evaluate(model,words,file,out):
    p,s = getCorrelation(model,words,file)
    return p,s

def evaluate_all(model,words):
    prefix = "../datasets_tokenized/"
    parr = []; sarr = []; farr = []

    farr = ["FNWN2013",#0
            #"JHUppdb",#1
            "MSRpar2012",#2
            "MSRvid2012",#3
            "OnWN2012",#4
            "OnWN2013",#5
            "OnWN2014",#6
            "SMT2013",#7
            "SMTeuro2012",#8
            "SMTnews2012",#9
            #"anno-dev",#10
            #"anno-test",#11
            "answer-forum2015",#12
            "answer-student2015",#13
            "belief2015",#14
            #"bigram-jn",#15
            #"bigram-nn",#16
            #"bigram-vn",#17
            "deft-forum2014",#18
            "deft-news2014",#19
            "headline2013",#20
            "headline2014",#21
            "headline2015",#22
            "images2014",#23
            "images2015",#24
            #"sicktest",#25
            "tweet-news2014"]#26
            #"twitter"]#27

    examples = []
    for i in farr:
        lis = getExamples(prefix+i)
        examples.extend(lis)

    print "Num examples: ", len(examples)

    fourorless = []; five = []; six = []; seven = []
    eight = []; nine = []; tenormore = []

    for i in examples:
        n = i[3]
        if n <= 4:
            fourorless.append(i)
        elif n <= 5:
            five.append(i)
        elif n == 6:
            six.append(i)
        elif n == 7:
            seven.append(i)
        elif n == 8:
            eight.append(i)
        elif n == 9:
            nine.append(i)
        else:
            tenormore.append(i)

    lis = []
    lis.append(fourorless)
    lis.append(five)
    lis.append(six)
    lis.append(seven)
    lis.append(eight)
    lis.append(nine)
    lis.append(tenormore)

    a = []
    for i in lis:
        s = getCorrelation(model,words,i)
        print len(i), s
        a.append(s)
    return a
