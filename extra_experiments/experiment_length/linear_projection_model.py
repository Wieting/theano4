
import theano
import numpy as np
from theano import tensor as T
from theano import config
import pdb
import time
import utils
import lasagne
from AverageLayer import AverageLayer
import sys
import cPickle

def checkIfQuarter(idx,n):
    #print idx, n
    if idx==round(n/4.) or idx==round(n/2.) or idx==round(3*n/4.):
        return True
    return False

class linear_projection_model(object):

    #takes list of seqs, puts them in a matrix
    #returns matrix of seqs and mask
    def prepare_data(self, list_of_seqs):
        lengths = [len(s) for s in list_of_seqs]
        n_samples = len(list_of_seqs)
        maxlen = np.max(lengths)
        x = np.zeros((n_samples, maxlen)).astype('int32')
        x_mask = np.zeros((n_samples, maxlen)).astype(theano.config.floatX)
        for idx, s in enumerate(list_of_seqs):
            x[idx, :lengths[idx]] = s
            x_mask[idx, :lengths[idx]] = 1.
        x_mask = np.asarray(x_mask,dtype=config.floatX)
        return x, x_mask

    def saveParams(self, fname):
        f = file(fname, 'wb')
        cPickle.dump(self.all_params, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()

    def get_minibatches_idx(self, n, minibatch_size, shuffle=False):
        idx_list = np.arange(n, dtype="int32")

        if shuffle:
            np.random.shuffle(idx_list)

        minibatches = []
        minibatch_start = 0
        for i in range(n // minibatch_size):
            minibatches.append(idx_list[minibatch_start:
                                    minibatch_start + minibatch_size])
            minibatch_start += minibatch_size

        if (minibatch_start != n):
            # Make a minibatch out of what is left
            minibatches.append(idx_list[minibatch_start:])

        return zip(range(len(minibatches)), minibatches)

    def getpairs(self, batch, params):
        #batch is list of tuples
        g1 = []; g2 = []
        for i in batch:
            g1.append(i[0].embeddings)
            g2.append(i[1].embeddings)

        g1x, g1mask = self.prepare_data(g1)
        g2x, g2mask = self.prepare_data(g2)

        embg1 = self.feedforward_function(g1x,g1mask)
        embg2 = self.feedforward_function(g2x,g2mask)

        #update representations
        for idx,i in enumerate(batch):
            i[0].representation = embg1[idx,:]
            i[1].representation = embg2[idx,:]

        #pairs = utils.getPairs(batch, params.type)
        pairs = utils.getPairsFast(batch, params.type)
        p1 = []; p2 = []
        for i in pairs:
            p1.append(i[0].embeddings)
            p2.append(i[1].embeddings)

        p1x, p1mask = self.prepare_data(p1)
        p2x, p2mask = self.prepare_data(p2)

        return (g1x,g1mask,g2x,g2mask,p1x,p1mask,p2x,p2mask)

#We, params.layersize, params.LC, params.LW,
#                                  params.updateword, params.eta, params.peephole, params.outgate)

    def __init__(self, layersize, nl, regfile):

        self.layersize = layersize
        self.nl = nl

        #params
        p = cPickle.load(file(regfile, 'rb'))
        We = theano.shared(np.asarray(p[0].get_value(), dtype = config.floatX))
        W = np.asarray(p[1].get_value(), dtype = config.floatX)
        b = np.asarray(p[2].get_value(), dtype = config.floatX)

        #symbolic params
        g1batchindices = T.imatrix(); g2batchindices = T.imatrix()
        p1batchindices = T.imatrix(); p2batchindices = T.imatrix()
        g1mask = T.matrix(); g2mask = T.matrix()
        p1mask = T.matrix(); p2mask = T.matrix()

        #get embeddings
        l_in = lasagne.layers.InputLayer((None, None))
        l_mask = lasagne.layers.InputLayer(shape=(None, None))
        l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=We.get_value().shape[0], output_size=We.get_value().shape[1], W=We)
        l_average = AverageLayer([l_emb, l_mask])
        l_proj = lasagne.layers.DenseLayer(l_average, layersize, nonlinearity=nl,W=W, b=b)

        embg1 = lasagne.layers.get_output(l_proj, {l_in:g1batchindices, l_mask:g1mask})
        embg2 = lasagne.layers.get_output(l_proj, {l_in:g2batchindices, l_mask:g2mask})

        g1g2 = (embg1*embg2).sum(axis=1)
        g1g2norm = T.sqrt(T.sum(embg1**2,axis=1)) * T.sqrt(T.sum(embg2**2,axis=1))
        g1g2 = g1g2 / g1g2norm

        prediction = g1g2
        self.scoring_function = theano.function([g1batchindices, g2batchindices,
                             g1mask, g2mask],prediction)