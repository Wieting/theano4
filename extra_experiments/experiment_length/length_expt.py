import evaluate
import utils
from word_model import word_model
from lstm_ppdb_model import lstm_ppdb_model
from linear_projection_model import linear_projection_model
import lasagne

(words, We) = utils.getWordmap("../data/paragram_sl999.txt")
model = word_model("../data/word-jhu-pickle.pickle")
p1 = evaluate.evaluate_all(model,words)

print ""
model = word_model("../data/word-sl-pickle.pickle")
p5 = evaluate.evaluate_all(model,words)

print ""
model = lstm_ppdb_model(300, True, False, "../data/lstm-nooutgate-jhu-pickle.pickle")
p4 = evaluate.evaluate_all(model,words)

print ""
model = lstm_ppdb_model(300, True, True, "../data/lstm-outgate-jhu-pickle.pickle")
p3 = evaluate.evaluate_all(model,words)

print ""
model = linear_projection_model(300, lasagne.nonlinearities.linear, "../data/proj-jhu-pickle.pickle")
p2 = evaluate.evaluate_all(model,words)

for i in range(len(p1)):
    print "%.3f & %.3f & %.3f & %.3f & %.3f\\\\" % (p1[i],p2[i],p3[i],p4[i],p5[i])