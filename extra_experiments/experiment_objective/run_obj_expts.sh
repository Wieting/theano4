python obj.py -batchsize 100 -hiddensize 300 -wordfile ../data/paragram_sl999.txt  -wordstem simlex -nntype lstm -layersize 300 -dataf ../data/data-time-plot/ppdb-XL-ordered-data.txt -samplingtype RAND -peephole True -outgate True -regfile ../data/lstm-outgate-jhu-pickle.pickle

python obj.py -batchsize 100 -hiddensize 300 -wordfile ../data/paragram_sl999.txt  -wordstem simlex -nntype lstm -layersize 300 -dataf ../data/data-time-plot/ppdb-XL-ordered-data.txt -samplingtype RAND -peephole True -outgate False -regfile ../data/lstm-nooutput-jhu-pickle.pickle

python obj.py -batchsize 100 -hiddensize 300 -wordfile ../data/paragram_sl999.txt  -wordstem simlex -nntype word -dataf ../data/data-time-plot/ppdb-XL-ordered-data.txt -samplingtype RAND -regfile ../data/word-jhu-pickle.pickle

python obj.py -batchsize 100 -hiddensize 300 -wordfile ../data/paragram_sl999.txt  -nonlinearity 1 -wordstem simlex -nntype proj -layersize 300  -dataf ../data/data-time-plot/ppdb-XL-ordered-data.txt -samplingtype RAND -regfile ../data/proj-jhu-pickle.pickle
