from glob import glob

class dataset(object):
    def __init__(self, d):
        d = d.strip().split()
        self.name = d[-1]
        self.p = float(d[0])
        if len(d) == 3:
            self.s = float(d[1])

class evaluation(object):
    def __init__(self,line):
        self.result = line
        line = line.split("|")
        self.datasets = {}
        for i in line:
            if len(i.split()) > 0:
                d = dataset(i)
                self.datasets[d.name]=d

    def getTunedResults(self):
         return self.datasets['JHUppdb'].s

    def printResults(self):
        print self.result

def getScore(flist):
    dd = {}

    for j in flist:
        f=open(j,'r')
        lines = f.readlines()
        best=-1; keep=-1
        for i in lines:
            if 'command:' in i:
                name = i
            if 'JHUppdb' in i:
                e = evaluation(i)
                t = e.getTunedResults()
                if t > best:
                    best = t
                    dd[name] = (best,e.result)

    #sort dicts
    print len(dd)
    print len(flist)
    dd_sorted=sorted(dd.items(), key=lambda x: x[1][0], reverse=True)

    for i in dd_sorted:
        print i[1][1]

flist = glob("training_phrases/SGE/sh.o*")
getScore(flist)