from glob import glob

class dataset(object):
    def __init__(self, d):
        d = d.strip().split()
        self.name = d[-1]
        self.p = float(d[0])
        self.s = float(d[1])

class evaluation(object):
    def __init__(self,line):
        self.result = line
        line = line.split("|")
        self.datasets = {}
        for i in line:
            if len(i.split()) > 0:
                d = dataset(i)
                self.datasets[d.name]=d

    def getTunedResults(self):
         return self.datasets['JHUppdb'].s

    def printResults(self):
        print self.result

def getScore(flist):
    dd = {}
    for j in flist:
        f=open(j,'r')
        lines = f.readlines()
        best=-1
        for i in lines:
            l = i
            if 'command:' in i:
                name = i
            if 'evaluation' in i:
                i=i.split()
                s = float(i[1])
                t = float(i[3])
                if s > best:
                    dd[name] = (s,l)
                    best = s

    #sort dicts
    print len(dd)
    print len(flist)
    dd_sorted=sorted(dd.items(), key=lambda x: x[1][0], reverse=True)

    for i in dd_sorted:
        print i

flist = glob("fine-tuning/SGE/sh.o*")
getScore(flist)