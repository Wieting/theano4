
#train word model
if [ "$1" == "similarity" ]; then
    sh train_classify.sh -wordstem simlex -wordfile ../data/sl999_XXL_data.txt -outfile gpu-word-model -updatewords True -hiddensize 300 -traindata ../datasets_tokenized/sicktrain -devdata ../datasets_tokenized/sickdev -testdata ../datasets_tokenized/sicktest -layersize 300 -save False -nntype word -evaluate True -epochs 10 -minval 1 -maxval 5 -traintype normal -task sim -batchsize 25 -LW 1e-05 -LC 1e-06 -memsize 50 -learner adam -eta 0.001
#train word model
elif [ "$1" == "entailment" ]; then
    sh train_classify.sh -wordstem simlex -wordfile ../data/sl999_XXL_data.txt -outfile gpu-word-model -updatewords True -hiddensize 300 -traindata ../datasets_tokenized/sicktrain-ent -devdata ../datasets_tokenized/sickdev-ent -testdata ../datasets_tokenized/sicktest-ent -layersize 300 -save False -nntype word -evaluate True -epochs 10 -traintype normal -task ent -batchsize 25 -LW 1e-05 -LC 1e-06 -memsize 150 -learner adagrad -eta 0.05
#train lstm w/ outgate
elif [ "$1" == "sentiment" ]; then
    sh train_classify.sh -wordstem simlex -wordfile ../data/sl999_XXL_data.txt -outfile gpu-lstm-model -hiddensize 300 -traindata ../datasets_tokenized/sentiment-train -devdata ../datasets_tokenized/sentiment-dev -testdata ../datasets_tokenized/sentiment-test -layersize 300 -save False -nntype lstm_sentiment -evaluate True -epochs 10 -peephole True -traintype normal -task sentiment -updatewords True -outgate True -batchsize 25 -LW 1e-06 -LC 1e-06 -memsize 300 -learner adam -eta 0.001
#train ppdb models
elif [ "$1" == "ppdb-lstm-outgate" ]; then
    sh train.sh -wordstem simlex -wordfile ../data/sl999_XXL_data.txt -outfile lstm-model-outgate -hiddensize 300 -dataf ../data/ppdb-XL-ordered-data.txt -layersize 300 -save False -nntype lstm -peephole True -evaluate True -epochs 10 -outgate True -updatewords True -batchsize 100 -LW 1e-06 -margin 0.4 -samplingtype MAX -LC 0.001 -clip 0 -eta 0.0005 -learner adam
elif [ "$1" == "ppdb-lstm-nooutgate" ]; then
    sh train.sh -wordstem simlex -wordfile ../data/sl999_XXL_data.txt -outfile lstm-model-nooutgate -hiddensize 300 -dataf ../data/ppdb-XL-ordered-data.txt -layersize 300 -save False -nntype lstm -peephole True -evaluate True -epochs 10 -outgate False -updatewords True -batchsize 50 -LW 1e-06 -margin 0.4 -samplingtype MAX -LC 0.001 -clip 0 -eta 0.0005 -learner adam
elif [ "$1" == "ppdb-proj" ]; then
    sh train.sh -wordstem simlex -wordfile ../data/sl999_XXL_data.txt -outfile proj-model -hiddensize 300 -dataf ../data/ppdb-XL-ordered-data.txt -layersize 300 -save False -nntype proj -evaluate True -epochs 10 -nonlinearity 1 -updatewords True -batchsize 100 -LW 1e-08 -margin 0.4 -samplingtype MAX -LC 1e-05 -clip 0 -eta 0.0005 -learner adam
elif [ "$1" == "ppdb-word" ]; then
    sh train.sh -wordstem simlex -wordfile ../data/sl999_XXL_data.txt -outfile word-model -updatewords True -hiddensize 300 -dataf ../data/ppdb-XL-ordered-data.txt -layersize 300 -save False -nntype word -evaluate True -epochs 10 -batchsize 100 -LW 1e-06 -margin 0.4 -samplingtype MAX -clip 1 -eta 0.005 -learner adam
elif [ "$1" == "ppdb-irnn" ]; then
    sh train.sh -wordstem simlex -wordfile ../data/sl999_XXL_data.txt -outfile irnn-model -hiddensize 300 -dataf ../data/ppdb-XL-ordered-data.txt -layersize 300 -save False -nntype rnn -evaluate True -epochs 10 -updatewords True -batchsize 100 -LW 1e-06 -margin 0.4 -samplingtype MAX -LC 10.0 -clip 1 -eta 0.005 -learner adam -add_rnn True
elif [ "$1" == "ppdb-rnn" ]; then
    sh train.sh -wordstem simlex -wordfile ../data/sl999_XXL_data.txt -outfile rnn-model -hiddensize 300 -dataf ../data/ppdb-XL-ordered-data.txt -layersize 300 -save False -nntype rnn -evaluate True -epochs 10 -add_rnn False -updatewords True -batchsize 100 -LW 1e-06 -margin 0.4 -samplingtype MIX -LC 0.0001 -clip 0 -eta 0.0005 -learner adam -nonlinearity 2
elif [ "$1" == "ppdb-dan" ]; then
    sh train.sh -wordstem simlex -wordfile ../data/sl999_XXL_data.txt -outfile dan-model -hiddensize 300 -dataf ../data/ppdb-XL-ordered-data.txt -layersize 300 -save False -nntype dan -evaluate True -epochs 10 -updatewords True -batchsize 100 -LW 1e-05 -margin 0.4 -samplingtype MAX -LC 1e-05 -clip 0 -eta 0.05 -learner adagrad -nonlinearity 2 -numlayers 1
elif [ "$1" == "ppdb-big" ]; then
    sh train.sh -wordstem simlex -wordfile ../data/sl999_XXL_data.txt -outfile big-model-4800 -hiddensize 300 -dataf ../data/ppdb-XL-ordered-data.txt -layersize 300 -save False -nntype big -evaluate True -epochs 10 -updatewords True -batchsize 100 -LW 1e-06 -margin 0.4 -samplingtype MAX -LC 1e-07 -clip 1 -eta 0.05 -learner adagrad -big 4800
else
    echo "$1 not a valid option."
fi