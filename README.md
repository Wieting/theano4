# iclr2016

Code to train models from "Towards Universal Paraphrastic Sentence Embeddings."

The code requires Theano as well as the lasagne library.

To get started run fetch.sh to download initial word embeddings and PPDB training data. There is a demo script that takes the model that you would like to train as a command line argument (check the script to see available choices). The script will reproduce the results from the paper for that model. Check main_code/ppdb_train.py and main_code/_train.py for command line options.

The code is separated into 3 parts:

entsim_code: contains code for training models on the SICK similarity and entailment code
main_code: contains code for training models on PPDB data as well as various utilities
senti_code: contains code for training sentiment models.

Note that only the Pavlick PPDB dataset, Annotated-PPDB from our TACL paper, the SICK dataset, and the sentiment dataset are included and tokenized.

If you use our code for your work please cite:

@article{wieting2016iclr,
author    = {John Wieting and
Mohit Bansal and
Kevin Gimpel and
Karen Livescu},
title     = {Towards Universal Paraphrastic Sentence Embeddings},
journal   = {CoRR},
volume    = {abs/1511.08198},
year      = {2015},
bibsource = {dblp computer science bibliography, http://dblp.org}
}