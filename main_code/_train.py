
from params import params
import utils
import lasagne
import random
import numpy as np
import sys
import argparse

new_path = '../entsim_code'
if new_path not in sys.path:
    sys.path.append(new_path)
new_path = '../senti_code'
if new_path not in sys.path:
    sys.path.append(new_path)

from _lstm_model import _lstm_model
from _proj_model import _proj_model
from _word_model import _word_model
from _dan_model import _dan_model
from _rnn_model import _rnn_model
from _big_model import _big_model
from _lstm_model_sentiment import _lstm_model_sentiment
from _proj_model_sentiment import _proj_model_sentiment
from _word_model_sentiment import _word_model_sentiment
from _dan_model_sentiment import _dan_model_sentiment
from _rnn_model_sentiment import _rnn_model_sentiment
from _big_model_sentiment import _big_model_sentiment
import _utils

def str2bool(v):
    if v is None:
        return False
    if v.lower() in ("yes", "true", "t", "1"):
        return True
    if v.lower() in ("no", "false", "f", "0"):
        return False
    raise ValueError('A type that was supposed to be boolean is not boolean.')

def learner2bool(v):
    if v is None:
        return lasagne.updates.adam
    if v.lower() == "adagrad":
        return lasagne.updates.adagrad
    if v.lower() == "adam":
        return lasagne.updates.adam
    raise ValueError('A type that was supposed to be a learner is not.')



random.seed(1)
np.random.seed(1)

params = params()

parser = argparse.ArgumentParser()
parser.add_argument("-LW", help="Regularization on Words", type=float)
parser.add_argument("-LC", help="Regularization on Composition Parameters", type=float)
parser.add_argument("-outfile", help="Output file name")
parser.add_argument("-batchsize", help="Size of batch", type=int)
parser.add_argument("-hiddensize", help="Size of input", type=int)
parser.add_argument("-memsize", help="Size of Memory layer. Only used when trained for classification.",
                    type=int)
parser.add_argument("-wordfile", help="Word file to be read in.")
parser.add_argument("-layersize", help="Size of Output layer.", type=int)
parser.add_argument("-updatewords", help="Whether to update the word embeddings")
parser.add_argument("-wordstem", help="Nickname of word embeddings used.")
parser.add_argument("-save", help="Whether to pickle the model.")
parser.add_argument("-traindata", help="Training data file.")
parser.add_argument("-devdata", help="Training data file.")
parser.add_argument("-testdata", help="Testing data file.")
parser.add_argument("-margin", help="Regularization on Words", type=float)
parser.add_argument("-samplingtype", help="Type of Sampling used.")
parser.add_argument("-peephole", help="Whether to use peepholes in LSTM.")
parser.add_argument("-outgate", help="Whether to use output gate in LSTM.")
parser.add_argument("-nonlinearity", help="Type of nonlinearity in projection model.",
                    type=int)
parser.add_argument("-nntype", help="Type of neural network.")
parser.add_argument("-evaluate", help="Whether to evaluate the model during training.")
parser.add_argument("-epochs", help="Number of epochs in training.", type=int)
parser.add_argument("-regfile", help="Path to model file that we want to regularize towards.")
parser.add_argument("-minval", help="min rating possible in scoring.", type=int)
parser.add_argument("-maxval", help="max rating in scoring.", type=int)
parser.add_argument("-LRW", help="lambda for regularization word parameters", type=float)
parser.add_argument("-LRC", help="lambda for regularization composition parameters", type=float)
parser.add_argument("-traintype", help="Either normal, reg, or rep.")
parser.add_argument("-clip", help="float to indicate grad cutting or False")
parser.add_argument("-eta", help="learning rate", type=float)
parser.add_argument("-learner", help="Either AdaGrad or Adam")
parser.add_argument("-task", help="Either sim, ent, or sentiment")
parser.add_argument("-add_rnn", help="Whether to keep RNN close to an addition model")
parser.add_argument("-numlayers", help="Number of layers in DAN Model", type=int)
parser.add_argument("-big", help="Size of big projection", type=int)

args = parser.parse_args()

params.LW = args.LW
params.LC = args.LC
params.outfile = args.outfile
params.batchsize = args.batchsize
params.hiddensize = args.hiddensize
params.memsize = args.memsize
params.wordfile = args.wordfile
params.nntype = args.nntype
params.layersize = args.layersize
params.updatewords = str2bool(args.updatewords)
params.wordstem = args.wordstem
params.save = str2bool(args.save)
params.traindata = args.traindata
params.devdata = args.devdata
params.testdata = args.testdata
params.margin = args.margin
params.samplingtype = args.samplingtype
params.usepeep = str2bool(args.peephole)
params.useoutgate = str2bool(args.outgate)
params.nntype = args.nntype
params.epochs = args.epochs
params.traintype = args.traintype
params.evaluate = str2bool(args.evaluate)
params.LRW = args.LRW
params.LRC = args.LRC
params.learner = learner2bool(args.learner)
params.task = args.task
params.add_rnn = str2bool(args.add_rnn)
params.numlayers = args.numlayers
params.big = args.big

if args.eta:
    params.eta = args.eta

params.clip = args.clip
if args.clip:
    if params.clip == 0:
        params.clip = None

params.regfile = args.regfile
params.minval = args.minval
params.maxval = args.maxval

if args.nonlinearity:
    if args.nonlinearity == 1:
        params.nonlinearity = lasagne.nonlinearities.linear
    if args.nonlinearity == 2:
        params.nonlinearity = lasagne.nonlinearities.tanh
    if args.nonlinearity == 3:
        params.nonlinearity = lasagne.nonlinearities.rectify
    if args.nonlinearity == 4:
        params.nonlinearity = lasagne.nonlinearities.sigmoid

(words, We) = utils.getWordmap(params.wordfile)

if args.task == "sim" or args.task == "ent":
    train_data = utils.getSimEntDataset(params.traindata,words,params.task)
elif args.task == "sentiment":
    train_data = utils.getSentimentDataset(params.traindata,words)
else:
    raise ValueError('Task should be ent or sim.')

model = None

print sys.argv

if params.nntype == 'lstm':
    model = _lstm_model(We, params)
elif params.nntype == 'word':
    model = _word_model(We, params)
elif params.nntype == 'proj':
    model = _proj_model(We, params)
elif params.nntype == 'dan':
    model = _dan_model(We, params)
elif params.nntype == 'rnn':
    model = _rnn_model(We, params)
elif params.nntype == 'big':
    model = _big_model(We, params)
elif params.nntype == 'lstm_sentiment':
    model = _lstm_model_sentiment(We, params)
elif params.nntype == 'word_sentiment':
    model = _word_model_sentiment(We, params)
elif params.nntype == 'proj_sentiment':
    model = _proj_model_sentiment(We, params)
elif params.nntype == 'dan_sentiment':
    model = _dan_model_sentiment(We, params)
elif params.nntype == 'rnn_sentiment':
    model = _rnn_model_sentiment(We, params)
elif params.nntype == 'big_sentiment':
    model = _big_model_sentiment(We, params)
else:
    "Error no type specified"

_utils.train(model, train_data, params.devdata, params.testdata, params.traindata, words, params)
