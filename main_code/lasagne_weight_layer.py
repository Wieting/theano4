
import lasagne
import theano

class lasagne_weight_layer(lasagne.layers.MergeLayer):
    
    def __init__(self, incoming, **kwargs):
        super(lasagne_weight_layer, self).__init__(incoming, **kwargs)
    
    def get_output_for(self, inputs, **kwargs):
        emb = inputs[0]
        wt = inputs[1]
        wt = theano.tensor.extra_ops.repeat(wt,emb.shape[2],2)
        prod = wt*emb
        #pdb.set_trace()
        return prod
    
    def get_output_shape_for(self, input_shape):
        #print input_shape
        return (input_shape[0][0],input_shape[0][2])