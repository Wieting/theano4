from utils import getWordmap
from params import params
from utils import getPPDBData
from ppdb_lstm_model import ppdb_lstm_model
from ppdb_rnn_model import ppdb_rnn_model
from ppdb_word_model import ppdb_word_model
from ppdb_weighted_word_model import ppdb_weighted_word_model
from ppdb_proj_model import ppdb_proj_model
from ppdb_dan_model import ppdb_dan_model
from ppdb_word_big_model import ppdb_word_big_model
import lasagne
import random
import numpy as np
import sys
import argparse
import utils

def str2bool(v):
    if v is None:
        return False
    if v.lower() in ("yes", "true", "t", "1"):
        return True
    if v.lower() in ("no", "false", "f", "0"):
        return False
    raise ValueError('A type that was supposed to be boolean is not boolean.')

def learner2bool(v):
    if v is None:
        return lasagne.updates.adagrad
    if v.lower() == "adagrad":
        return lasagne.updates.adagrad
    if v.lower() == "adam":
        return lasagne.updates.adam
    raise ValueError('A type that was supposed to be a learner is not.')

random.seed(1)
np.random.seed(1)

params = params()

parser = argparse.ArgumentParser()
parser.add_argument("-LW", help="Regularization on Words", type=float)
parser.add_argument("-LC", help="Regularization on Composition Parameters", type=float)
parser.add_argument("-outfile", help="Output file name")
parser.add_argument("-batchsize", help="Size of batch", type=int)
parser.add_argument("-hiddensize", help="Size of input", type=int)
parser.add_argument("-memsize", help="Size of Memory layer. Only used when trained for classification.",
                    type=int)
parser.add_argument("-wordfile", help="Word file to be read in.")
parser.add_argument("-layersize", help="Size of Output layer.", type=int)
parser.add_argument("-updatewords", help="Whether to update the word embeddings")
parser.add_argument("-wordstem", help="Nickname of word embeddings used.")
parser.add_argument("-save", help="Whether to pickle the model.")
parser.add_argument("-dataf", help="Training data file.")
parser.add_argument("-margin", help="Regularization on Words", type=float)
parser.add_argument("-samplingtype", help="Type of Sampling used.")
parser.add_argument("-peephole", help="Whether to use peepholes in LSTM.")
parser.add_argument("-outgate", help="Whether to use output gate in LSTM.")
parser.add_argument("-nonlinearity", help="Type of nonlinearity in projection model.",
                    type=int)
parser.add_argument("-nntype", help="Type of neural network.")
parser.add_argument("-evaluate", help="Whether to evaluate the model during training.")
parser.add_argument("-epochs", help="Number of epochs in training.", type=int)
parser.add_argument("-fraction", help="Percent of training examples to use.", type=float)
parser.add_argument("-clip", help="float to indicate grad cutting. Use 0 for no clipping.",type=int)
parser.add_argument("-eta", help="learning rate",type=float)
parser.add_argument("-learner", help="Either AdaGrad or Adam")
parser.add_argument("-add_rnn", help="Whether to keep RNN close to an addition model")
parser.add_argument("-numlayers", help="Number of layers in DAN Model", type=int)
parser.add_argument("-big", help="Size of big projection", type=int)

args = parser.parse_args()

params.LW = args.LW
params.LC = args.LC
params.outfile = args.outfile
params.batchsize = args.batchsize
params.hiddensize = args.hiddensize
params.wordfile = args.wordfile
params.nntype = args.nntype
params.layersize = args.layersize
params.updatewords = str2bool(args.updatewords)
params.wordstem = args.wordstem
params.save = str2bool(args.save)
params.dataf = args.dataf
params.margin = args.margin
params.type = args.samplingtype
params.peephole = str2bool(args.peephole)
params.outgate = str2bool(args.outgate)
params.nntype = args.nntype
params.epochs = args.epochs
params.evaluate = str2bool(args.evaluate)
params.learner = learner2bool(args.learner)
params.add_rnn = str2bool(args.add_rnn)
params.numlayers = args.numlayers
params.big = args.big

if args.eta:
    params.eta = args.eta

params.clip = args.clip
if args.clip:
    if params.clip == 0:
        params.clip = None

if args.nonlinearity:
    if args.nonlinearity == 1:
        params.nonlinearity = lasagne.nonlinearities.linear
    if args.nonlinearity == 2:
        params.nonlinearity = lasagne.nonlinearities.tanh
    if args.nonlinearity == 3:
        params.nonlinearity = lasagne.nonlinearities.rectify
    if args.nonlinearity == 4:
        params.nonlinearity = lasagne.nonlinearities.sigmoid

(words, We) = getWordmap(params.wordfile)
wordfilestem = params.wordfile.split("/")[-1].replace(".txt", "")
out = "../models/" + params.outfile

params.outfile = out
examples = getPPDBData(params.dataf, words)

if args.fraction:
    examples = examples[0:int(round(args.fraction * len(examples)))]

print "Saving to: " + params.outfile

model = None

print sys.argv
print params

if params.nntype == 'lstm':
    model = ppdb_lstm_model(We, params)
elif params.nntype == 'word':
    model = ppdb_word_model(We, params)
elif params.nntype == 'proj':
    model = ppdb_proj_model(We, params)
elif params.nntype == 'wtword':
    model = ppdb_weighted_word_model(We, params)
elif params.nntype == 'rnn':
    model = ppdb_rnn_model(We, params)
elif params.nntype == 'dan':
    model = ppdb_dan_model(We, params)
elif params.nntype == 'big':
    model = ppdb_word_big_model(We, params)
else:
    "Error no type specified"

utils.train(model, examples, words, params)
