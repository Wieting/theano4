
import lasagne
from theano import tensor as T

class lasagne_dot_layer(lasagne.layers.Layer):

    def __init__(self, incoming, num_units, W=lasagne.init.GlorotUniform(),
                 b=lasagne.init.Constant(0.), nonlinearity=lasagne.nonlinearities.identity,
                 **kwargs):
        super(lasagne_dot_layer, self).__init__(incoming, **kwargs)
        self.nonlinearity = nonlinearity

        self.num_units = num_units

        num_inputs = self.input_shape[-1]

        self.W = self.add_param(W, (num_inputs, num_units), name="W_dot")
        self.b = self.add_param(b, (num_units,), name="b_dot",
                                    regularizable=False)

    def get_output_shape_for(self, input_shape):
        return (input_shape[0], input_shape[1], self.num_units)

    def get_output_for(self, input, **kwargs):
        activation = T.dot(input, self.W)
        activation = activation + self.b.dimshuffle('x', 0)
        return self.nonlinearity(activation)
