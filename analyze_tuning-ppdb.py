from glob import glob

class dataset(object):
    def __init__(self, d):
        d = d.strip().split()
        self.name = d[-1]
        self.p = float(d[0])
        if len(d) == 3:
            self.s = float(d[1])

class evaluation(object):
    def __init__(self,line):
        self.result = line
        line = line.split("|")
        self.datasets = {}
        for i in line:
            if len(i.split()) > 0:
                d = dataset(i)
                self.datasets[d.name]=d

    def getTunedResults(self):
         return self.datasets['JHUppdb'].s

    def printResults(self):
        print self.result

def getBestModel(flist, eval_dataset, key):
    dd = {}

    for j in flist:
        f=open(j,'r')
        lines = f.readlines()
        best=-1; keep=-1
        for i in lines:
            if 'command:' in i:
                name = i
                if key not in name:
                    break
            if 'JHUppdb' in i:
                e = evaluation(i)
                t = e.datasets[eval_dataset].s
                if t > best:
                    best = t
                    dd[name] = (best,e.result)

    #sort dicts
    print len(dd)
    print len(flist)
    dd_sorted=sorted(dd.items(), key=lambda x: x[1][0], reverse=True)
    return dd_sorted[0]

def processCommand(s):
    cmd = s[0].strip()
    cmd = cmd.replace('command: ','')
    print cmd

flist = glob("training_phrases/SGE/sh.o*")
m = getBestModel(flist, 'JHUppdb', '-outgate True')
#print m
processCommand(m)
m = getBestModel(flist, 'JHUppdb', '-outgate False')
#print m
processCommand(m)
m = getBestModel(flist, 'JHUppdb', '-nntype proj')
#print m
processCommand(m)
m = getBestModel(flist, 'JHUppdb', '-nntype word')
#print m
processCommand(m)
m = getBestModel(flist, 'anno-test', '-outgate True')
#print m
processCommand(m)
m = getBestModel(flist, 'anno-test', '-outgate False')
#print m
processCommand(m)
m = getBestModel(flist, 'anno-test', '-nntype proj')
#print m
processCommand(m)
m = getBestModel(flist, 'anno-test', '-nntype word')
#print m
processCommand(m)